tool
extends VBoxContainer

export(String, MULTILINE) var text := "" setget setText #todo figure out how to format this in realtime

#todo setters for each of these
export var _defaultFont:DynamicFont
export var _italicFont:DynamicFont
export var _boldFont:DynamicFont
export var _monoFont:DynamicFont
export var _fontSize = 16 setget setFontSize

var defaultFontSet:fontSet = fontSet.new()

var headers = {}

const headerDefaultMults = {
	"# ": 2.0,
	"## ": 1.5,
	"### ": 1.17,
	"#### ": 1.0,
	"##### ": 0.83,
	"###### ": 0.67
}


func _ready():
	yield(get_tree().create_timer(.1), "timeout")
	rebuild()
#		headers[label] = null

func rebuild():
	resetFonts()
	setText(text)



func setText(value:String):
#	if value == text:
#		return
	text = value
	
	for i in get_children():
		(i as Node).queue_free()
	
	var buffer = [];
	
	var lines := text.split("\n");
	for line in lines:
		line = line.strip_edges()
		if line == "":
			var nodes= processBuffer(buffer)
			for node in nodes:
				add_child(node)
			buffer = []
		else:
			buffer.push_back(line)
#	processBuffer(buffer)


func processBuffer(buffer:PoolStringArray):
	if buffer.size() == 0:
		return []
	if buffer[0].substr(0,1) == "-":
		return buildBufferAsList(buffer)

	var flatString:String = ""
	for i in buffer:
		if i.substr(i.length()-1,1) == "\\": #todo detect literal \\
			i.erase(i.length()-1,1)
			flatString += i + "\n"
		else:
			flatString += i + " "
	flatString = consumeInlineFormatting(flatString)
	#create line
	var node = RichTextLabel.new()
	node.bbcode_enabled = true
	node.fit_content_height = true
#	node.autowrap = true
	defaultFontSet._applyFontSet(node)
	
	#format header
	for headerPrefix in headers:
		if flatString.begins_with(headerPrefix):
			flatString = flatString.substr(headerPrefix.length())
			(headers[headerPrefix] as fontSet)._applyFontSet(node)
			break

	#strip excess whitespace
	var changed:=true
	while changed:
		var original = flatString
		changed = false
		flatString = flatString.replace("  "," ")
		flatString = flatString.replace("	"," ")

	node.bbcode_text = flatString
	node.connect("meta_clicked",self,"metaClicked")
	
	return [node]
	
func buildBufferAsList(buffer:PoolStringArray):
	var output = []

func setFontSize(value):
	_fontSize = value
	rebuild()

class fontSet:
	var _normal_font
	var _bold
	var _italic
	var _bold_italic
	var _monospace
	func _resized(newSize:int)->fontSet:
		var a:fontSet = fontSet.new()
		for i in ["_normal_font","_bold","_italic","_bold_italic","_monospace"]:
			if self[i]:
				a[i] = self[i].duplicate()
				a[i].size = newSize
			else:
				a[i] = null
		return a;
	func _applyFontSet(rtl:RichTextLabel):
		rtl.set("custom_fonts/normal_font",_normal_font)
		rtl.set("custom_fonts/bold_font",_bold)
		rtl.set("custom_fonts/italics_font",_italic)
		rtl.set("custom_fonts/bold_italics_font",_bold_italic)
		rtl.set("custom_fonts/mono_font",_monospace)

func resetFonts():
	defaultFontSet._normal_font = _defaultFont
	defaultFontSet._bold = _boldFont
	defaultFontSet._italic = _italicFont
	defaultFontSet._bold_italic = null #todo
	defaultFontSet._monospace = _monoFont
	defaultFontSet = defaultFontSet._resized(_fontSize)
	
	headers = {}
	for label in headerDefaultMults:
		headers[label] = defaultFontSet._resized(_fontSize * headerDefaultMults[label])


func consumeInlineFormatting(input:String)->String:
	var output:String = ""
	
	var i = 0
	var insideStrike:bool = false
	var insideBold:bool = false
	var insideItalic:bool = false


	var url = ["\\[(.+?)\\]\\((.+?)\\)","[url=$2]$1[/url]"]
	var regex:=RegEx.new()
	regex.compile(url[0])
	input = regex.sub(input,url[1],true)
	#todo figure out how to skip url parsing when inside code block

	var strikethrough = ["~~(.+?)~~","[s]$1[/s]"]
	regex.compile(strikethrough[0])
	input = regex.sub(input,strikethrough[1],true)
	#start with code formatting!
	while i < input.length():
		var temp = input.substr(i)
		if input[i] == "`":
			input.erase(i,1)
			input = input.insert(i,"[code]")
			i += 6
			while input.substr(i,1) != "`" and i < input.length():
				i += 1
			input.erase(i,1)
			input = input.insert(i,"[/code]")
			i += 7
		
#		if input.substr(i,2) == "~~":
#			input.erase(i,2)
#			if insideStrike:
#				input = input.insert(i,"[/s]")
#				i += 4;
#			else:
#				input = input.insert(i,"[s]")
#				i += 3;
#			insideStrike = !insideStrike

		if input.substr(i,2) == "**":
			input.erase(i,2)
			if insideBold:
				input = input.insert(i,"[/b]")
				i += 4;
			else:
				input = input.insert(i,"[b]")
				i += 3;
			insideBold = !insideBold

		if input.substr(i,1) == "*":
			var t = input.substr(-2,3)
			if input.substr(i-1,2) == "\\*":
				input.erase(i-1,1)
			else:
				input.erase(i,1)
				if insideItalic:
					input = input.insert(i,"[/i]")
					i += 4;
				else:
					input = input.insert(i,"[i]")
					i += 3;
				insideItalic = !insideItalic

		i += 1

	return input

func strikeThrough(input:String)->String:
	var output:String = ""
	
	var inside = true
	for i in input.split("~~"):
		output += i
		if inside:
			output += "[s]"
		else:
			output += "[/s]"
		inside = !inside
	
	return output;

func metaClicked(data):
	OS.shell_open(data)
