# Godot Markdown Formatter

This is **Godot Markdown Formatter**. It's a tool for converting simple text input into a Godot scene tree. Markdown was originally created for to make writing text in a terminal easy to write *and* read, but still look good on the web with proper `<HTML>` tags.

I'm writing this because I use Markdown for *everything*. I take notes in Markdown. I do design work in Markdown. I write documentation in Markdown.

Markdown is very widely supported an a version of Markdown lives in Discord, Reddit, and other modern social platforms. 

Since the intended use for Markdown is in HTML, I will use `<>` to describe what various formatting tricks do in Markdown.

## First, Headers

So far, you've seen 3 different font sizes. Unlike BBCode, Markdown lets you have section headers very easily. The top section header is a `<h1>` with `#` and the section section header is a `<h2>` with `##`. Up to `<h6>` with `######` is supported.

## New Line

You'll notice I'm double spacing new line.
But when I start a new line without an empty space, the output is on the same line as the previous line.
For Markdown, you need a double space to get a new line (`<br>`).
This makes it easier to read on a text editor as you can put line breaks
anywhere
you
want
but on a web browser with more dynamic widths, it still looks good.

If you want a *literal* new line:\
just end your line with a `\`

## Inline Text Formatting

Sometimes you want to *emphasize* things. Sometimes you **really** want to *emphasize*. ~~And sometimes you want to take stuff back.~~ Use `*` like quotation marks around bocks of text you want to italicize, `**` for bold, and `~~` for strike through.

Use backticks (usually left of the 1 key on most keyboards) around text you want to make monospaced (usually used for code).

## Lists

- This is a bulleted list.
- It's useful for making a list of stuff
including stuff on more than one line
- Use bulleted lists when ordering doesn't matter

// todo numbered lists\
// todo sublists

## Links!

We can support links! Here is a link to [GitHub's Markdown guide](https://guides.github.com/features/mastering-markdown/).


[GitLab Repo](https://gitlab.com/greenfox/markdown-project/)

[Dev Build](https://greenfox.gitlab.io/markdown-project/)

[Dev Web Build](https://greenfox.gitlab.io/markdown-project/HTML5/The%20Markdown%20Project.html)

[Itch Page](https://greenf0x.itch.io/the-markdown-project)

### In Engine Links

I would really like to be able to trigger GDScript from links. Not 100% sure *how* I'm going to do that... We'll see...

`[Maybe something like this?](function://nodepath:funcName)`

## Todo

This is a work in progress. Here are things that don't work, along with likelihood that I will add them.

- Ordered Lists (90%)
- Multilayered Lists (75%)
- Task List (75%)
- Image Imbedding (50%)
- Blockquotes (50%)
- Multiline Code (75%)
    - Syntax Highlighting (20%)
- Tables (25%)
- EMOJI! (0%)
